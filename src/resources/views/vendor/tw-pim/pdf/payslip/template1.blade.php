<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }
        table, th, td {
            padding: .3rem;
        }
        .header-title {
            text-align: center;
        }
        .header-title h3 {
            margin-top: 2rem;
            margin-bottom: .5rem;
        }
        .header-title h4 {
            margin: .5rem;
        }
        .table-right {
            text-align: right;
        }
        .table-center {
            text-align: center;
        }
        .table-border-top {
            border-top: 2px solid black;
        }
        .table-border-left {
            border-left: 2px solid black;
        }
        ul li {
            list-style-type: none;
        }
        .main-container {
            margin: 4rem;
        }
    </style>
</head>
<body>
    <div class="main-container">
        <table>
            <tr>
                <td colspan="9" class="header-title">
                    <h3>United Rebuilders, Inc.</h3>
                    <h4>www.unitedrebuilders.com</h4>
                </td>
            </tr>
            <tr class="border-top">
                <td colspan="2">Payslip Period: </td>
                <td colspan="3">{{ $payroll->payroll_log->start->format('M. d, Y') }} - {{ $payroll->payroll_log->end->format('M. d, Y') }}</td>
                <td colspan="4" rowspan="3" class="border-left">Shift TwoPairs- 8a to 5p | Payslip# {{ $payroll->id }}</td>
            </tr>
            <tr>
                <td colspan="2">Pay Date: </td>
                <td colspan="3">2018-06-15</td>
            </tr>
            <tr>
                <td colspan="2">Pay Coverage: </td>
                <td colspan="3">{{ $payroll->payroll_log->start->format('M. d, Y') }} - {{ $payroll->payroll_log->end->format('M. d, Y') }}</td>
            </tr>
            <tr>
                <td colspan="2">Name: </td>
                <td colspan="3">{{ $payroll->name }}</td>
                <td colspan="4" rowspan="11" class="border-left border-top" style="vertical-align: top;">
                    <ul style="padding-inline-start: 3px;">
                        @foreach($payroll->employee->timelogs as $timelog)
                            <li>{{ $timelog->id }}</li>
                        @endforeach

                        <li>05/31 - [Proj. OT]</li>
                        <li>05/31 - [Proj. OT]</li>
                        <li>05/31 - [Proj. OT]</li>
                    </ul>
                </td>
            </tr>
            <tr class="border-top">
                <td colspan="2">Emp# {{ $payroll->employee->employee_id_number }} Bio# {{ $payroll->employee->bio_id }}</td>
                <td colspan="3">
                    <p class="right">Position: {{ $payroll->employee->job_position->name }}</p>
                </td>
            </tr>
            <tr>
                <td colspan="2">Civil Status: MF3/S3</td>
                <td colspan="3">
                    <p class="right">Rate: 373</p>
                </td>
            </tr>
            <tr class="border-top">
                <td colspan="2">
                    <p class="right">(12) Basic P:</p>
                </td>
                <td>
                    <p class="right">4,476.00</p>
                </td>
                <td rowspan="2">
                    <p class="right">OT&RD#</p>
                </td>
                <td rowspan="2">
                    <p class="right">107h</p>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <p class="right">RestDay P:</p>
                </td>
                <td>
                    <p class="right">969.00</p>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <p class="right">Holiday P:</p>
                </td>
                <td>
                    <p class="right">373.00</p>
                </td>
                <td class="border-top">
                    <p class="right">Pag-ibig Loan:</p>
                </td>
                <td class="border-top">
                    <p class="right">P -1,143.43</p>
                </td>          
            </tr>
            <tr>
                <td colspan="2">
                    <p class="right">OT P:</p>
                    </td>
                <td>
                    <p class="right">373.00</p>
                </td>
                <td>
                    <p class="right">Pag-ibig</p>
                </td>
                <td>
                    <p class="right">P -100.00</p>
                </td> 
            </tr>
            <tr>
                <td colspan="2">
                    <p class="right">OT Night:</p>
                </td>
                <td>
                    <p class="right">373.00</p>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <p class="right">Gross:</p>
                </td>
                <td>
                    <p class="right">373.00</p>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="border-top">
                    <p class="right">Daily Extra:</p>
                    </td>
                <td class="border-top">
                    <p class="right">373.00</p>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <p class="right">Loyalty Incentive:</p>
                </td>
                <td>
                    <p class="right">373.00</p>
                </td>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td colspan="2" class="border-top">
                    <p class="right">Total Bonus P:</p>
                </td>
                <td class="border-top">
                    <p class="right">373.00</p>
                </td>
                <td colspan="2"></td>
                <td colspan="4" class="border-left border-top">
                    <p class="center">RECEIVED BY:</p>
                </td>
            </tr>
            <tr style="height: 3rem;">
                <td colspan="5">
                    <p class="center">REMARKS:[1 T.C][+RH -0d][+OT -2h]</p>
                </td>
                <td colspan="4" class="border-left"></td>
            </tr>
            <tr style="height: 3rem;">
                <td colspan="2" class="border-top">
                    <p class="right">NET PAY:</p>
                </td>
                <td colspan="3" class="border-top">
                    <p class="right">P 10,404.49</p>
                </td>
                <td colspan="4" class="border-left">
                    <p style="border-top: 1px solid black; width: 50%; margin-left: 25%;"></p>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>