<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function(Blueprint $table) {
            $table->increments('id')->unsigned();

            // AUTH
            $table->string('password')->nullable();
            $table->string('remember_token')->nullable();

            // PERSONAL
            $table->string('employee_id_number')->nullable(); // USERNAME
            $table->integer('title_id')->unsigned()->nullable();
            $table->string('first_name');
            $table->string('last_name');

            $table->boolean('gender')->nullable()->default(true);
            $table->date('birthdate')->nullable();
            $table->integer('civil_status_id')->unsigned()->nullable();
            $table->string('nationality')->nullable();
            $table->string('religion')->nullable();

            // CONTACT
            $table->string('email')->nullable();
            $table->string('landline')->nullable();
            $table->string('mobile')->nullable();

            // ADDRESS
            $table->string('address_1')->nullable();
            $table->string('address_2')->nullable();
            $table->string('city')->nullable();
            $table->string('province')->nullable();
            $table->string('zipcode')->nullable();
            $table->string('country')->nullable();

            // EMPLOYMENT
            $table->integer('job_position_id')->unsigned()->nullable();
            $table->integer('department_id')->unsigned()->nullable();
            $table->timestamp('date_hired')->nullable();
            $table->timestamp('date_dismissed')->nullable();

            // GOVT
            $table->integer('tax_status_id')->unsigned()->nullable();

            // OTHERS
            $table->text('notes')->nullable();
            $table->string('status')->nullable();

            $table->softDeletes();
            $table->timestamps();

            $table->index('department_id');
            $table->foreign('department_id')
                ->references('id')
                ->on('company_departments');
            
            $table->index('job_position_id');
            $table->foreign('job_position_id')
                ->references('id')
                ->on('job_positions');

            $table->index('civil_status_id');
            $table->foreign('civil_status_id')
                ->references('id')
                ->on('civil_status');

            $table->index('title_id');
            $table->foreign('title_id')
                ->references('id')
                ->on('titles');

            $table->index('tax_status_id');
            $table->foreign('tax_status_id')
                ->references('id')
                ->on('tax_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employees', function($table) 
        {
            $table->dropForeign('employees_job_position_id_foreign');
            $table->dropIndex('employees_job_position_id_index');

            $table->dropForeign('employees_department_id_foreign');
            $table->dropIndex('employees_department_id_index');

            $table->dropForeign('employees_civil_status_id_foreign');
            $table->dropIndex('employees_civil_status_id_index');

            $table->dropForeign('employees_title_id_foreign');
            $table->dropIndex('employees_title_id_index');

            $table->dropForeign('employees_tax_status_id_foreign');
            $table->dropIndex('employees_tax_status_id_index');
        });

        Schema::drop('employees');
    }
}
