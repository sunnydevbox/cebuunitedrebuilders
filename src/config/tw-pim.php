<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Table name
    |--------------------------------------------------------------------------
    |
    | table name
    |
    */
    'table'=>'employees',

    /*
    |--------------------------------------------------------------------------
    | Employee Model
    |--------------------------------------------------------------------------
    |
    | Developer can override this with his class that extends 
    | the main Employee Model
    |
    */
    'models' => [
        'employee'              => \Sunnydevbox\CebuUnitedRebuilders\Models\Employee::class,
        'employee-leave-credit' => \Sunnydevbox\TWPim\Models\EmployeeLeaveCredit::class,
        'employee-log'          => \Sunnydevbox\TWPim\Models\EmployeeLog::class,
        'employee-template'     => \Sunnydevbox\TWPim\Models\EmployeeTemplate::class,
        'holiday'               => \Sunnydevbox\TWPim\Models\Holiday::class,
        'holiday-type'          => \Sunnydevbox\TWPim\Models\HolidayType::class,
        'shift-template'        => \Sunnydevbox\TWPim\Models\ShiftTemplate::class,
        'shift-template-period' => \Sunnydevbox\TWPim\Models\ShiftTemplatePeriod::class,
        'deduction'             => \Sunnydevbox\TWPim\Models\Deduction::class,
        'employee-deduction'    => \Sunnydevbox\TWPim\Models\EmployeeDeduction::class,
        'timelog'               => \Sunnydevbox\CebuUnitedRebuilders\Models\Timelog::class,
        'timelog-import'        => \Sunnydevbox\TWPim\Models\TimelogImport::class,
        'leave-type'            => \Sunnydevbox\TWPim\Models\LeaveType::class,
        'leave-application'     => \Sunnydevbox\TWPim\Models\LeaveApplication::class,
        'payroll'               => \Sunnydevbox\TWPim\Models\Payroll::class,
        'payroll-item'          => \Sunnydevbox\TWPim\Models\PayrollItem::class,
        'payroll-template'      => \Sunnydevbox\TWPim\Models\PayrollTemplate::class,
        'payroll-log'           => \Sunnydevbox\TWPim\Models\PayrollLog::class,
        'benefit'               => \Sunnydevbox\TWPim\Models\Benefit::class,
        'system-variable'       => \Sunnydevbox\TWPim\Models\SystemVariable::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Employee Role
    |--------------------------------------------------------------------------
    |
    | Default role to assign to new employee records
    |
    */
    'role' => 'employee',

    /*
    |--------------------------------------------------------------------------
    | Employee ID Generator
    |--------------------------------------------------------------------------
    |
    | ID Generator class
    |
    */
    'id_generator' => \Sunnydevbox\CebuUnitedRebuilders\Services\EmployeeIDService::class,


    /*
    |--------------------------------------------------------------------------
    | Employee ID Generator
    |--------------------------------------------------------------------------
    |
    | ID Generator class
    |
    */
    'dtr_importers' => [

    ],

];
