<?php return array (
  'auth' => 
  array (
    'defaults' => 
    array (
      'guard' => 'web',
      'passwords' => 'users',
    ),
    'guards' => 
    array (
      'web' => 
      array (
        'driver' => 'session',
        'provider' => 'users',
      ),
      'api' => 
      array (
        'driver' => 'jwt',
        'provider' => 'users',
      ),
      'employee' => 
      array (
        'driver' => 'jwt',
        'provider' => 'employees',
      ),
    ),
    'providers' => 
    array (
      'users' => 
      array (
        'driver' => 'eloquent',
        'model' => 'Sunnydevbox\\CebuUnitedRebuilders\\Models\\User',
      ),
      'employees' => 
      array (
        'driver' => 'eloquent',
        'model' => 'Sunnydevbox\\TWPim\\Models\\Employee',
      ),
    ),
    'passwords' => 
    array (
      'users' => 
      array (
        'provider' => 'users',
        'table' => 'password_resets',
        'expire' => 60,
      ),
    ),
  ),
  'app' => 
  array (
    'name' => 'Laravel',
    'env' => 'local',
    'debug' => true,
    'url' => 'http://localhost',
    'timezone' => 'UTC',
    'locale' => 'en',
    'fallback_locale' => 'en',
    'key' => 'base64:U6EM41SZnhhXg4J+BKA8vmRM7i/NmM4NVd9+v+H4hIU=',
    'cipher' => 'AES-256-CBC',
    'log' => 'daily',
    'log_level' => 'debug',
    'providers' => 
    array (
      0 => 'Illuminate\\Auth\\AuthServiceProvider',
      1 => 'Illuminate\\Broadcasting\\BroadcastServiceProvider',
      2 => 'Illuminate\\Bus\\BusServiceProvider',
      3 => 'Illuminate\\Cache\\CacheServiceProvider',
      4 => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
      5 => 'Illuminate\\Cookie\\CookieServiceProvider',
      6 => 'Illuminate\\Database\\DatabaseServiceProvider',
      7 => 'Illuminate\\Encryption\\EncryptionServiceProvider',
      8 => 'Illuminate\\Filesystem\\FilesystemServiceProvider',
      9 => 'Illuminate\\Foundation\\Providers\\FoundationServiceProvider',
      10 => 'Illuminate\\Hashing\\HashServiceProvider',
      11 => 'Illuminate\\Mail\\MailServiceProvider',
      12 => 'Illuminate\\Notifications\\NotificationServiceProvider',
      13 => 'Illuminate\\Pagination\\PaginationServiceProvider',
      14 => 'Illuminate\\Pipeline\\PipelineServiceProvider',
      15 => 'Illuminate\\Queue\\QueueServiceProvider',
      16 => 'Illuminate\\Redis\\RedisServiceProvider',
      17 => 'Illuminate\\Auth\\Passwords\\PasswordResetServiceProvider',
      18 => 'Illuminate\\Session\\SessionServiceProvider',
      19 => 'Illuminate\\Translation\\TranslationServiceProvider',
      20 => 'Illuminate\\Validation\\ValidationServiceProvider',
      21 => 'Illuminate\\View\\ViewServiceProvider',
      22 => 'Laravel\\Tinker\\TinkerServiceProvider',
      23 => 'App\\Providers\\AppServiceProvider',
      24 => 'App\\Providers\\AuthServiceProvider',
      25 => 'App\\Providers\\EventServiceProvider',
      26 => 'App\\Providers\\RouteServiceProvider',
      27 => 'Sunnydevbox\\CebuUnitedRebuilders\\CebuUnitedRebuildersServiceProvider',
      28 => 'Barryvdh\\DomPDF\\ServiceProvider',
    ),
    'aliases' => 
    array (
      'App' => 'Illuminate\\Support\\Facades\\App',
      'Artisan' => 'Illuminate\\Support\\Facades\\Artisan',
      'Auth' => 'Illuminate\\Support\\Facades\\Auth',
      'Blade' => 'Illuminate\\Support\\Facades\\Blade',
      'Broadcast' => 'Illuminate\\Support\\Facades\\Broadcast',
      'Bus' => 'Illuminate\\Support\\Facades\\Bus',
      'Cache' => 'Illuminate\\Support\\Facades\\Cache',
      'Config' => 'Illuminate\\Support\\Facades\\Config',
      'Cookie' => 'Illuminate\\Support\\Facades\\Cookie',
      'Crypt' => 'Illuminate\\Support\\Facades\\Crypt',
      'DB' => 'Illuminate\\Support\\Facades\\DB',
      'Eloquent' => 'Illuminate\\Database\\Eloquent\\Model',
      'Event' => 'Illuminate\\Support\\Facades\\Event',
      'File' => 'Illuminate\\Support\\Facades\\File',
      'Gate' => 'Illuminate\\Support\\Facades\\Gate',
      'Hash' => 'Illuminate\\Support\\Facades\\Hash',
      'Lang' => 'Illuminate\\Support\\Facades\\Lang',
      'Log' => 'Illuminate\\Support\\Facades\\Log',
      'Mail' => 'Illuminate\\Support\\Facades\\Mail',
      'Notification' => 'Illuminate\\Support\\Facades\\Notification',
      'Password' => 'Illuminate\\Support\\Facades\\Password',
      'Queue' => 'Illuminate\\Support\\Facades\\Queue',
      'Redirect' => 'Illuminate\\Support\\Facades\\Redirect',
      'Redis' => 'Illuminate\\Support\\Facades\\Redis',
      'Request' => 'Illuminate\\Support\\Facades\\Request',
      'Response' => 'Illuminate\\Support\\Facades\\Response',
      'Route' => 'Illuminate\\Support\\Facades\\Route',
      'Schema' => 'Illuminate\\Support\\Facades\\Schema',
      'Session' => 'Illuminate\\Support\\Facades\\Session',
      'Storage' => 'Illuminate\\Support\\Facades\\Storage',
      'URL' => 'Illuminate\\Support\\Facades\\URL',
      'Validator' => 'Illuminate\\Support\\Facades\\Validator',
      'View' => 'Illuminate\\Support\\Facades\\View',
      'PDF' => 'Barryvdh\\DomPDF\\Facade',
    ),
  ),
  'mail' => 
  array (
    'driver' => 'smtp',
    'host' => 'smtp.mailtrap.io',
    'port' => '2525',
    'from' => 
    array (
      'address' => 'hello@example.com',
      'name' => 'Example',
    ),
    'encryption' => NULL,
    'username' => NULL,
    'password' => NULL,
    'sendmail' => '/usr/sbin/sendmail -bs',
    'markdown' => 
    array (
      'theme' => 'default',
      'paths' => 
      array (
        0 => '/application/resources/views/vendor/mail',
      ),
    ),
  ),
  'dompdf' => 
  array (
    'show_warnings' => false,
    'orientation' => 'portrait',
    'defines' => 
    array (
      'DOMPDF_FONT_DIR' => '/application/storage/fonts/',
      'DOMPDF_FONT_CACHE' => '/application/storage/fonts/',
      'DOMPDF_TEMP_DIR' => '/tmp',
      'DOMPDF_CHROOT' => '/application',
      'DOMPDF_UNICODE_ENABLED' => true,
      'DOMPDF_ENABLE_FONT_SUBSETTING' => false,
      'DOMPDF_PDF_BACKEND' => 'CPDF',
      'DOMPDF_DEFAULT_MEDIA_TYPE' => 'screen',
      'DOMPDF_DEFAULT_PAPER_SIZE' => 'a4',
      'DOMPDF_DEFAULT_FONT' => 'serif',
      'DOMPDF_DPI' => 96,
      'DOMPDF_ENABLE_PHP' => false,
      'DOMPDF_ENABLE_JAVASCRIPT' => true,
      'DOMPDF_ENABLE_REMOTE' => true,
      'DOMPDF_FONT_HEIGHT_RATIO' => 1.1,
      'DOMPDF_ENABLE_CSS_FLOAT' => false,
      'DOMPDF_ENABLE_HTML5PARSER' => false,
    ),
  ),
  'services' => 
  array (
    'mailgun' => 
    array (
      'domain' => NULL,
      'secret' => NULL,
    ),
    'ses' => 
    array (
      'key' => NULL,
      'secret' => NULL,
      'region' => 'us-east-1',
    ),
    'sparkpost' => 
    array (
      'secret' => NULL,
    ),
    'stripe' => 
    array (
      'model' => 'App\\User',
      'key' => NULL,
      'secret' => NULL,
    ),
  ),
  'database' => 
  array (
    'default' => 'mysql',
    'connections' => 
    array (
      'sqlite' => 
      array (
        'driver' => 'sqlite',
        'database' => 'cebuunitedrebuileders',
        'prefix' => '',
      ),
      'mysql' => 
      array (
        'driver' => 'mysql',
        'host' => 'cebuunitedrebuilders-mysql',
        'port' => '3306',
        'database' => 'cebuunitedrebuileders',
        'username' => 'root',
        'password' => 'root',
        'unix_socket' => '',
        'charset' => 'utf8mb4',
        'collation' => 'utf8mb4_unicode_ci',
        'prefix' => '',
        'strict' => false,
        'engine' => NULL,
      ),
      'pgsql' => 
      array (
        'driver' => 'pgsql',
        'host' => 'cebuunitedrebuilders-mysql',
        'port' => '3306',
        'database' => 'cebuunitedrebuileders',
        'username' => 'root',
        'password' => 'root',
        'charset' => 'utf8',
        'prefix' => '',
        'schema' => 'public',
        'sslmode' => 'prefer',
      ),
      'sqlsrv' => 
      array (
        'driver' => 'sqlsrv',
        'host' => 'cebuunitedrebuilders-mysql',
        'port' => '3306',
        'database' => 'cebuunitedrebuileders',
        'username' => 'root',
        'password' => 'root',
        'charset' => 'utf8',
        'prefix' => '',
      ),
    ),
    'migrations' => 'migrations',
    'redis' => 
    array (
      'client' => 'predis',
      'default' => 
      array (
        'host' => '127.0.0.1',
        'password' => NULL,
        'port' => '6379',
        'database' => 0,
      ),
    ),
  ),
  'cache' => 
  array (
    'default' => 'file',
    'stores' => 
    array (
      'apc' => 
      array (
        'driver' => 'apc',
      ),
      'array' => 
      array (
        'driver' => 'array',
      ),
      'database' => 
      array (
        'driver' => 'database',
        'table' => 'cache',
        'connection' => NULL,
      ),
      'file' => 
      array (
        'driver' => 'file',
        'path' => '/application/storage/framework/cache/data',
      ),
      'memcached' => 
      array (
        'driver' => 'memcached',
        'persistent_id' => NULL,
        'sasl' => 
        array (
          0 => NULL,
          1 => NULL,
        ),
        'options' => 
        array (
        ),
        'servers' => 
        array (
          0 => 
          array (
            'host' => '127.0.0.1',
            'port' => 11211,
            'weight' => 100,
          ),
        ),
      ),
      'redis' => 
      array (
        'driver' => 'redis',
        'connection' => 'default',
      ),
    ),
    'prefix' => 'laravel',
  ),
  'session' => 
  array (
    'driver' => 'file',
    'lifetime' => 120,
    'expire_on_close' => false,
    'encrypt' => false,
    'files' => '/application/storage/framework/sessions',
    'connection' => NULL,
    'table' => 'sessions',
    'store' => NULL,
    'lottery' => 
    array (
      0 => 2,
      1 => 100,
    ),
    'cookie' => 'laravel_session',
    'path' => '/',
    'domain' => NULL,
    'secure' => false,
    'http_only' => true,
  ),
  'attachments' => 
  array (
    'routes' => 
    array (
      'publish' => true,
      'prefix' => 'attachments',
      'middleware' => 'web',
      'pattern' => '/{id}/{name}',
      'shared_pattern' => '/shared/{token}',
      'dropzone' => 
      array (
        'upload_pattern' => '/dropzone',
        'delete_pattern' => '/dropzone/{id}',
      ),
    ),
    'uuid_provider' => 'uuid_v4_base36',
    'behaviors' => 
    array (
      'cascade_delete' => true,
      'dropzone_check_csrf' => true,
    ),
    'attributes' => 
    array (
      0 => 'title',
      1 => 'description',
      2 => 'key',
      3 => 'disk',
      4 => 'group',
    ),
    'storage_directory' => 
    array (
      'prefix' => 'attachments',
    ),
  ),
  'log-viewer' => 
  array (
    'storage-path' => '/application/storage/logs',
    'pattern' => 
    array (
      'prefix' => 'laravel-',
      'date' => '[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]',
      'extension' => '.log',
    ),
    'locale' => 'auto',
    'route' => 
    array (
      'enabled' => true,
      'attributes' => 
      array (
        'prefix' => 'log-viewer',
        'middleware' => NULL,
      ),
    ),
    'per-page' => 30,
    'facade' => 'LogViewer',
    'download' => 
    array (
      'prefix' => 'laravel-',
      'extension' => 'log',
    ),
    'menu' => 
    array (
      'filter-route' => 'log-viewer::logs.filter',
      'icons-enabled' => true,
    ),
    'icons' => 
    array (
      'all' => 'fa fa-fw fa-list',
      'emergency' => 'fa fa-fw fa-bug',
      'alert' => 'fa fa-fw fa-bullhorn',
      'critical' => 'fa fa-fw fa-heartbeat',
      'error' => 'fa fa-fw fa-times-circle',
      'warning' => 'fa fa-fw fa-exclamation-triangle',
      'notice' => 'fa fa-fw fa-exclamation-circle',
      'info' => 'fa fa-fw fa-info-circle',
      'debug' => 'fa fa-fw fa-life-ring',
    ),
    'colors' => 
    array (
      'levels' => 
      array (
        'empty' => '#D1D1D1',
        'all' => '#8A8A8A',
        'emergency' => '#B71C1C',
        'alert' => '#D32F2F',
        'critical' => '#F44336',
        'error' => '#FF5722',
        'warning' => '#FF9100',
        'notice' => '#4CAF50',
        'info' => '#1976D2',
        'debug' => '#90CAF9',
      ),
    ),
    'highlight' => 
    array (
      0 => '^#\\d+',
      1 => '^Stack trace:',
    ),
  ),
  'laravel-activitylog' => 
  array (
    'enabled' => true,
    'delete_records_older_than_days' => 365,
    'default_log_name' => 'default',
    'default_auth_driver' => NULL,
    'subject_returns_soft_deleted_models' => false,
    'activity_model' => 'Spatie\\Activitylog\\Models\\Activity',
  ),
  'queue' => 
  array (
    'default' => 'sync',
    'connections' => 
    array (
      'sync' => 
      array (
        'driver' => 'sync',
      ),
      'database' => 
      array (
        'driver' => 'database',
        'table' => 'jobs',
        'queue' => 'default',
        'retry_after' => 90,
      ),
      'beanstalkd' => 
      array (
        'driver' => 'beanstalkd',
        'host' => 'localhost',
        'queue' => 'default',
        'retry_after' => 90,
      ),
      'sqs' => 
      array (
        'driver' => 'sqs',
        'key' => 'your-public-key',
        'secret' => 'your-secret-key',
        'prefix' => 'https://sqs.us-east-1.amazonaws.com/your-account-id',
        'queue' => 'your-queue-name',
        'region' => 'us-east-1',
      ),
      'redis' => 
      array (
        'driver' => 'redis',
        'connection' => 'default',
        'queue' => 'default',
        'retry_after' => 90,
      ),
    ),
    'failed' => 
    array (
      'database' => 'mysql',
      'table' => 'failed_jobs',
    ),
  ),
  'tw-user' => 
  array (
    'admin' => 
    array (
      'users' => 
      array (
        'integrate' => false,
        'section' => 'Sunnydevbox\\TWUser\\Admin\\Http\\Sections\\Users',
        'title' => 'Users',
        'icon' => 'fa-users',
        'model' => 'Sunnydevbox\\TWUser\\Models\\User',
        'alias' => 'users',
      ),
      'roles' => 
      array (
        'integrate' => false,
        'section' => 'Sunnydevbox\\TWUser\\Admin\\Http\\Sections\\Roles',
        'title' => 'Roles',
        'icon' => 'fa-users',
        'model' => 'Sunnydevbox\\TWUser\\Models\\Role',
        'alias' => 'roles',
      ),
      'permissions' => 
      array (
        'integrate' => false,
        'section' => 'Sunnydevbox\\TWUser\\Admin\\Http\\Sections\\Permissions',
        'title' => 'Permissions',
        'icon' => 'fa-users',
        'model' => 'Sunnydevbox\\TWUser\\Models\\Permission',
        'alias' => 'permissions',
      ),
    ),
    'api' => 
    array (
      'users' => 
      array (
        'model' => 'Sunnydevbox\\CebuUnitedRebuilders\\Models\\User',
        'controller' => 'Sunnydevbox\\CebuUnitedRebuilders\\Http\\Controllers\\API\\V1\\UserController',
        'repository' => 'Sunnydevbox\\CebuUnitedRebuilders\\Repositories\\User\\UserRepository',
        'transformer' => 'Sunnydevbox\\CebuUnitedRebuilders\\Transformers\\User\\UserTransformer',
      ),
    ),
    'config' => 
    array (
      'laravel-permission' => 
      array (
        'models' => 
        array (
          'permission' => 'Spatie\\Permission\\Models\\Permission',
          'role' => 'Spatie\\Permission\\Models\\Role',
        ),
        'table_names' => 
        array (
          'users' => 'users',
          'roles' => 'roles',
          'permissions' => 'permissions',
          'user_has_permissions' => 'user_has_permissions',
          'user_has_roles' => 'user_has_roles',
          'role_has_permissions' => 'role_has_permissions',
        ),
        'foreign_keys' => 
        array (
          'users' => 'user_id',
        ),
        'log_registration_exception' => true,
      ),
    ),
  ),
  'broadcasting' => 
  array (
    'default' => 'log',
    'connections' => 
    array (
      'pusher' => 
      array (
        'driver' => 'pusher',
        'key' => '',
        'secret' => '',
        'app_id' => '',
        'options' => 
        array (
        ),
      ),
      'redis' => 
      array (
        'driver' => 'redis',
        'connection' => 'default',
      ),
      'log' => 
      array (
        'driver' => 'log',
      ),
      'null' => 
      array (
        'driver' => 'null',
      ),
    ),
  ),
  'tw-pim' => 
  array (
    'table' => 'employees',
    'models' => 
    array (
      'employee' => 'Sunnydevbox\\CebuUnitedRebuilders\\Models\\Employee',
      'employee-leave-credit' => 'Sunnydevbox\\TWPim\\Models\\EmployeeLeaveCredit',
      'employee-log' => 'Sunnydevbox\\TWPim\\Models\\EmployeeLog',
      'employee-template' => 'Sunnydevbox\\TWPim\\Models\\EmployeeTemplate',
      'holiday' => 'Sunnydevbox\\TWPim\\Models\\Holiday',
      'holiday-type' => 'Sunnydevbox\\TWPim\\Models\\HolidayType',
      'shift-template' => 'Sunnydevbox\\TWPim\\Models\\ShiftTemplate',
      'shift-template-period' => 'Sunnydevbox\\TWPim\\Models\\ShiftTemplatePeriod',
      'deduction' => 'Sunnydevbox\\TWPim\\Models\\Deduction',
      'employee-deduction' => 'Sunnydevbox\\TWPim\\Models\\EmployeeDeduction',
      'timelog' => 'Sunnydevbox\\CebuUnitedRebuilders\\Models\\Timelog',
      'timelog-import' => 'Sunnydevbox\\TWPim\\Models\\TimelogImport',
      'leave-type' => 'Sunnydevbox\\TWPim\\Models\\LeaveType',
      'leave-application' => 'Sunnydevbox\\TWPim\\Models\\LeaveApplication',
      'payroll' => 'Sunnydevbox\\TWPim\\Models\\Payroll',
      'payroll-item' => 'Sunnydevbox\\TWPim\\Models\\PayrollItem',
      'payroll-template' => 'Sunnydevbox\\TWPim\\Models\\PayrollTemplate',
      'payroll-log' => 'Sunnydevbox\\TWPim\\Models\\PayrollLog',
      'benefit' => 'Sunnydevbox\\TWPim\\Models\\Benefit',
      'system-variable' => 'Sunnydevbox\\TWPim\\Models\\SystemVariable',
    ),
    'role' => 'employee',
    'id_generator' => 'Sunnydevbox\\CebuUnitedRebuilders\\Services\\EmployeeIDService',
    'allowed_advanced_hours' => 1,
    'dtr_importers' => 
    array (
    ),
  ),
  'view' => 
  array (
    'paths' => 
    array (
      0 => '/application/resources/views',
    ),
    'compiled' => '/application/storage/framework/views',
  ),
  'swaggervel' => 
  array (
    'doc-dir' => '/application/storage/docs',
    'doc-route' => 'docs',
    'api-docs-route' => 'api/docs',
    'app-dir' => 'app',
    'excludes' => 
    array (
      0 => '/application/storage',
      1 => '/application/tests',
      2 => '/application/resources/views',
      3 => '/application/config',
      4 => '/application/vendor',
    ),
    'generateAlways' => true,
    'api-key' => 'auth_token',
    'default-api-version' => 'v1',
    'default-swagger-version' => '2.0',
    'default-base-path' => '',
    'behind-reverse-proxy' => false,
  ),
  'api' => 
  array (
    'standardsTree' => 'x',
    'subtype' => 'cebuunitedrebuilders',
    'version' => 'v1',
    'prefix' => 'api',
    'domain' => NULL,
    'name' => 'Cebu United Rebuilders Inc. API',
    'conditionalRequest' => true,
    'strict' => true,
    'debug' => true,
    'errorFormat' => 
    array (
      'message' => ':message',
      'errors' => ':errors',
      'code' => ':code',
      'status_code' => ':status_code',
      'debug' => ':debug',
    ),
    'middleware' => 
    array (
    ),
    'auth' => 
    array (
      'jwt' => 'Dingo\\Api\\Auth\\Provider\\JWT',
      'employee' => 'Sunnydevbox\\TWPim\\Providers\\EmployeeAuthProvider',
    ),
    'throttling' => 
    array (
    ),
    'transformer' => 'Dingo\\Api\\Transformer\\Adapter\\Fractal',
    'defaultFormat' => 'json',
    'formats' => 
    array (
      'json' => 'Dingo\\Api\\Http\\Response\\Format\\Json',
    ),
  ),
  'cors' => 
  array (
    'supportsCredentials' => false,
    'allowedOrigins' => 
    array (
      0 => '*',
    ),
    'allowedHeaders' => 
    array (
      0 => '*',
    ),
    'allowedMethods' => 
    array (
      0 => '*',
    ),
    'exposedHeaders' => 
    array (
    ),
    'maxAge' => 0,
  ),
  'permission' => 
  array (
    'models' => 
    array (
      'permission' => 'Sunnydevbox\\TWUser\\Models\\Permission',
      'role' => 'Sunnydevbox\\TWUser\\Models\\Role',
    ),
    'table_names' => 
    array (
      'roles' => 'roles',
      'permissions' => 'permissions',
      'model_has_permissions' => 'model_has_permissions',
      'model_has_roles' => 'model_has_roles',
      'role_has_permissions' => 'role_has_permissions',
    ),
    'column_names' => 
    array (
      'model_morph_key' => 'model_id',
    ),
    'display_permission_in_exception' => false,
    'cache' => 
    array (
      'expiration_time' => 1440,
      'key' => 'spatie.permission.cache',
      'model_key' => 'name',
      'store' => 'default',
    ),
    'cache_expiration_time' => 1440,
  ),
  'tinker' => 
  array (
    'commands' => 
    array (
    ),
    'dont_alias' => 
    array (
    ),
  ),
  'tw-core' => 
  array (
    'tables' => 
    array (
      'activity_log' => 'activity_log',
    ),
    'controllers' => 
    array (
      'activity_log' => 'Sunnydevbox\\TWCore\\Http\\Controllers\\ActivityLogController',
    ),
    'models' => 
    array (
      'activity_log' => 'Sunnydevbox\\TWCore\\Models\\ActivityLog',
    ),
    'repositories' => 
    array (
      'activity_log' => 'Sunnydevbox\\TWCore\\Repositories\\ActivityLogRepositories',
    ),
    'enable_log_viewer' => true,
  ),
  'jwt' => 
  array (
    'secret' => 'mhMcMV1tmHYssp3TKX9HoqXv3yROYUhz',
    'ttl' => 1440,
    'refresh_ttl' => 20160,
    'algo' => 'HS256',
    'user' => 'Sunnydevbox\\CebuUnitedRebuilders\\Models\\User',
    'identifier' => 'id',
    'required_claims' => 
    array (
      0 => 'iss',
      1 => 'iat',
      2 => 'exp',
      3 => 'nbf',
      4 => 'sub',
      5 => 'jti',
    ),
    'blacklist_enabled' => true,
    'providers' => 
    array (
      'user' => 'Tymon\\JWTAuth\\Providers\\User\\EloquentUserAdapter',
      'jwt' => 'Tymon\\JWTAuth\\Providers\\JWT\\NamshiAdapter',
      'auth' => 'Tymon\\JWTAuth\\Providers\\Auth\\IlluminateAuthAdapter',
      'storage' => 'Tymon\\JWTAuth\\Providers\\Storage\\IlluminateCacheAdapter',
    ),
  ),
  'repository' => 
  array (
    'pagination' => 
    array (
      'limit' => 15,
    ),
    'fractal' => 
    array (
      'params' => 
      array (
        'include' => 'include',
      ),
      'serializer' => 'League\\Fractal\\Serializer\\DataArraySerializer',
    ),
    'cache' => 
    array (
      'enabled' => false,
      'minutes' => 30,
      'repository' => 'cache',
      'clean' => 
      array (
        'enabled' => true,
        'on' => 
        array (
          'create' => true,
          'update' => true,
          'delete' => true,
        ),
      ),
      'params' => 
      array (
        'skipCache' => 'skipCache',
      ),
      'allowed' => 
      array (
        'only' => NULL,
        'except' => NULL,
      ),
    ),
    'criteria' => 
    array (
      'acceptedConditions' => 
      array (
        0 => '=',
        1 => 'like',
      ),
      'params' => 
      array (
        'search' => 'search',
        'searchFields' => 'searchFields',
        'filter' => 'filter',
        'orderBy' => 'orderBy',
        'sortedBy' => 'sortedBy',
        'with' => 'with',
        'searchJoin' => 'searchJoin',
      ),
    ),
    'generator' => 
    array (
      'basePath' => '/application/app',
      'rootNamespace' => 'App\\',
      'stubsOverridePath' => '/application/app',
      'paths' => 
      array (
        'models' => 'Entities',
        'repositories' => 'Repositories',
        'interfaces' => 'Repositories',
        'transformers' => 'Transformers',
        'presenters' => 'Presenters',
        'validators' => 'Validators',
        'controllers' => 'Http/Controllers',
        'provider' => 'RepositoryServiceProvider',
        'criteria' => 'Criteria',
      ),
    ),
  ),
  'filesystems' => 
  array (
    'default' => 'local',
    'cloud' => 's3',
    'disks' => 
    array (
      'local' => 
      array (
        'driver' => 'local',
        'root' => '/application/storage/app',
      ),
      'public' => 
      array (
        'driver' => 'local',
        'root' => '/application/storage/app/public',
        'url' => 'http://localhost/storage',
        'visibility' => 'public',
      ),
      's3' => 
      array (
        'driver' => 's3',
        'key' => NULL,
        'secret' => NULL,
        'region' => NULL,
        'bucket' => NULL,
      ),
    ),
  ),
  'cebuunitedrebuilders' => 
  array (
  ),
);
